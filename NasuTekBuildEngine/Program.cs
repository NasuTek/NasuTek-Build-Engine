﻿// Decompiled with JetBrains decompiler
// Type: NasuTekBuildEngine.Program
// Assembly: NasuTekBuildEngine, Version=7.1.0.1000, Culture=neutral, PublicKeyToken=null
// MVID: 4B00D810-D71F-4146-BCC8-AACCED9C0FA4
// Assembly location: C:\Users\mjmanley\Projects\NasuTekBuildEngine.exe

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Xml.Linq;

namespace NasuTekBuildEngine {
    internal class Program {
        private static void Main(string[] args) {
            Console.WriteLine("NasuTek Build Engine");
            Console.WriteLine("\tCopyright (C) 2017 NasuTek Enterprises");
            Console.WriteLine();
            Console.WriteLine("Version: " + (object)Assembly.GetExecutingAssembly().GetName().Version);
            Console.WriteLine();

            if (args.Length < 1) {
                Help(nameof(Main));
            } else {
                switch (args[0]) {
                    case "checkout":
                        Program.Checkout(args[1], "master");
                        Program.GenerateFiles(args[1]);
                        break;
                    case "generate":
                        Program.GenerateFiles(args[1]);
                        break;
                    case "git":
                        RunGitCommand(args);
                        break;
                    case "fetch": {
                        var list = new List<String>();
                        list.Add("git");
                        list.AddRange(args);
                        RunGitCommand(list.ToArray());
                    }
                        break;
                    case "pull": {
                        var list = new List<String>();
                        list.Add("git");
                        list.AddRange(args);
                        RunGitCommand(list.ToArray());
                    }
                        break;
                    default:
                        Help(nameof(Main));
                        break;
                }
            }
        }

        private static void Help(string topic) {
            if (!(topic == "Main"))
                return;
            Console.WriteLine("checkout <spec url>: Checkout any files needed for project and generates build files.");
            Console.WriteLine("generate <spec url>: Generates build files.");
            Console.WriteLine("git <git commands>: executes a git command on all git working directories");
            Console.WriteLine("fetch <fetch params>: alias to git fetch");
            Console.WriteLine("pull <pull params>: alias to git pull");
        }

        private static void Checkout(string s, string branch) {
            XDocument xdocument = XDocument.Load(s);
            Process.Start(new ProcessStartInfo("git", "clone https://github.com/NasuTek/BuildTools.git Tools") {
                    UseShellExecute = false
                })
                .WaitForExit();
            foreach (XElement element in xdocument.Element((XName)"BuildEngine").Elements((XName)"Library")) {
                Console.WriteLine("Downloading and Extracting Library Package " + element.Attribute((XName)"url").Value + "...");
                string tempFileName = Path.GetTempFileName();
                new WebClient().DownloadFile(element.Attribute((XName)"url").Value, tempFileName);
                ZipFile.ExtractToDirectory(tempFileName, element.Attribute((XName)"path").Value.Replace("$(MSBuildThisFileDirectory)", ""));
            }
            foreach (XElement element in xdocument.Element((XName)"BuildEngine").Elements((XName)"Git"))
                Process.Start(new ProcessStartInfo("git",
                        "clone " + element.Attribute((XName)"git_url").Value + " -b " + (element.Attribute((XName)"git_branch") != null ? element.Attribute((XName)"git_branch").Value : "master")) {
                        UseShellExecute = false
                    })
                    .WaitForExit();
            foreach (XElement element in xdocument.Element((XName)"BuildEngine").Elements((XName)"Project"))
                Process.Start(new ProcessStartInfo("git",
                        "clone " + element.Attribute((XName)"git_url").Value + " -b " + (element.Attribute((XName)"git_branch") != null ? element.Attribute((XName)"git_branch").Value : "master")) {
                        UseShellExecute = false
                    })
                    .WaitForExit();
        }

        private static void GenerateFiles(string s) {
            XDocument xdocument1 = XDocument.Load(s);
            XDocument xdocument2 = new XDocument();
            XDocument xdocument3 = new XDocument();
            List<XElement> xelementList = new List<XElement>();
            XNamespace xnamespace = (XNamespace)"http://schemas.microsoft.com/developer/msbuild/2003";
            xdocument3.Add((object)new XElement(xnamespace + "Project", (object)new XElement(xnamespace + "PropertyGroup")));
            xdocument2.Add((object)new XElement(xnamespace + "Project"));
            xdocument2.Element(xnamespace + "Project").Add((object)new XElement(xnamespace + "Import", (object)new XAttribute((XName)"Project", (object)"$(MSBuildThisFileDirectory)BuildConfig.proj")));
            XElement xelement1 = new XElement(xnamespace + "ItemGroup");
            if (xdocument1.Element((XName)"BuildEngine").Element((XName)"AutoGenEnable") != null) {
                xelement1.Add((object)new XElement(xnamespace + "Tools", (object)new XAttribute((XName)"Include", (object)"$(MSBuildThisFileDirectory)Tools\\AutoGen\\AutoGen.csproj")));
                xelement1.Element(xnamespace + "Tools").Add((object)new XElement(xnamespace + "Properties", (object)"Configuration=$(Configuration)"));
            }
            xdocument2.Element(xnamespace + "Project").Add((object)xelement1);
            XElement xelement2 = new XElement(xnamespace + "ItemGroup");
            foreach (XElement element in xdocument1.Element((XName)"BuildEngine").Elements((XName)"Varable"))
                xdocument3.Element(xnamespace + "Project")
                    .Element(xnamespace + "PropertyGroup")
                    .Add((object)new XElement(xnamespace + element.Attribute((XName)"id").Value, (object)element.Attribute((XName)"value").Value));
            foreach (XElement element in xdocument1.Element((XName)"BuildEngine").Elements((XName)"Project")) {
                XElement xelement3 = new XElement(xnamespace + "ProjectToBuild",
                    new object[2] {
                        (object)new XAttribute((XName)"Include", (object)element.Attribute((XName)"proj").Value),
                        (object)new XAttribute((XName)"Condition", (object)("'$(Build" + element.Attribute((XName)"id").Value + ")' == 'true'"))
                    });
                xelement3.Add((object)new XElement(xnamespace + "Properties", (object)"Configuration=$(Configuration)"));
                xdocument3.Element(xnamespace + "Project")
                    .Element(xnamespace + "PropertyGroup")
                    .Add((object)new XElement((XName)((xnamespace + "Build").ToString() + element.Attribute((XName)"id").Value), (object)"true"));
                xelement2.Add((object)xelement3);
                if (xdocument1.Element((XName)"BuildEngine").Element((XName)"AutoGenEnable") != null)
                    xelementList.Add(new XElement(xnamespace + "Exec",
                        new object[3] {
                            (object)new XAttribute((XName)"Command", (object)"$(MSBuildThisFileDirectory)Tools\\AutoGen\\bin\\$(Configuration)\\AutoGen.exe $(MSBuildThisFileDirectory)AutoGenSettings.xml"),
                            (object)new XAttribute((XName)"WorkingDirectory", (object)element.Attribute((XName)"path").Value),
                            (object)new XAttribute((XName)"Condition", (object)("'$(Build" + element.Attribute((XName)"id").Value + ")' == 'true'"))
                        }));
            }
            xdocument2.Element(xnamespace + "Project").Add((object)xelement2);
            if (xdocument1.Element((XName)"BuildEngine").Element((XName)"AutoGenEnable") != null) {
                XDocument xdocument4 = new XDocument();
                xdocument4.Add((object)xdocument1.Element((XName)"BuildEngine").Element((XName)"AutoGenEnable").Element((XName)"AutoGen"));
                xdocument4.Save(xdocument1.Element((XName)"BuildEngine").Element((XName)"AutoGenEnable").Attribute((XName)"file").Value);
            }
            foreach (XElement element in xdocument1.Element((XName)"BuildEngine").Elements((XName)"Library"))
                xdocument3.Element(xnamespace + "Project")
                    .Element(xnamespace + "PropertyGroup")
                    .Add((object)new XElement(xnamespace + element.Attribute((XName)"id").Value, (object)element.Attribute((XName)"path").Value));
            XElement xelement4 = new XElement(xnamespace + "Target", (object)new XAttribute((XName)"Name", (object)"Build"));
            xelement4.Add((object)new XElement(xnamespace + "MSBuild", (object)new XAttribute((XName)"Projects", (object)"@(Tools)")));
            foreach (XElement xelement3 in xelementList)
                xelement4.Add((object)xelement3);
            xelement4.Add((object)new XElement(xnamespace + "MSBuild", (object)new XAttribute((XName)"Projects", (object)"@(ProjectToBuild)")));
            xdocument2.Element(xnamespace + "Project").Add((object)xelement4);
            xdocument2.Save("Build.proj");
            xdocument3.Save("BuildConfig.proj");
            System.IO.File.WriteAllText("build_debug.cmd", "%SystemRoot%\\Microsoft.NET\\Framework\\v4.0.30319\\msbuild.exe Build.proj /p:Configuration=Debug");
            System.IO.File.WriteAllText("build_release.cmd", "%SystemRoot%\\Microsoft.NET\\Framework\\v4.0.30319\\msbuild.exe Build.proj /p:Configuration=Release");
        }

        private static void RunGitCommand(string[] args) {
            args[0] = "";

            var command = String.Join(" ", args).Substring(1);

            foreach (var dir in Directory.GetDirectories(Environment.CurrentDirectory).Where(v => Directory.Exists(Path.Combine(v, ".git")))) {
                var curdir = Environment.CurrentDirectory;
                Environment.CurrentDirectory = dir;
                Process.Start(new ProcessStartInfo("git", command) {
                        UseShellExecute = false
                    })
                    .WaitForExit();
                Environment.CurrentDirectory = curdir;
            }
        }
    }
}
