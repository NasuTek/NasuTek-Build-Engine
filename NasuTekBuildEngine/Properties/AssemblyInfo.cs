﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("NasuTek Build Engine")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("NasuTek Enterprises")]
[assembly: AssemblyProduct("NasuTek Build Engine")]
[assembly: AssemblyCopyright("Copyright © 2017 NasuTek Enterprises")]
[assembly: AssemblyTrademark("")]
[assembly: ComVisible(false)]
[assembly: Guid("5cdff639-b152-4bb2-8dd4-effa93fe9ad3")]
[assembly: AssemblyFileVersion("7.1.0.1001")]
[assembly: AssemblyVersion("7.1.0.1001")]
